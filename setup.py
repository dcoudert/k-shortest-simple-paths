"""
Setup
"""

from distutils.core import setup
#from setuptools import setup
from setuptools import Extension
from Cython.Build import cythonize
from glob import glob
import sys
import platform

if platform.uname()[4] == 'arm64':
    pari_path = "/opt/homebrew/Cellar/pari/2.15.3/lib/"
else:
    pari_path = ""


directives = {
    'optimize.inline_defnode_calls': True,
    'language_level': sys.version_info[0] # "2" or "3"
}

extensions = [
    #"sagemath/types.pyx",
    Extension("sagemath.types",
              glob("sagemath/types.pyx"),
              extra_compile_args=["-std=c++17"],
              library_dirs=[pari_path]),
    #"sagemath/pairing_heap.pyx",
    Extension("sagemath.pairing_heap",
              glob("sagemath/pairing_heap.pyx"),
              extra_compile_args=["-std=c++17"],
              library_dirs=[pari_path]),
    #"sagemath/digraph.pyx",
    Extension("sagemath.digraph",
              glob("sagemath/digraph.pyx"),
              extra_compile_args=["-std=c++17"],
              library_dirs=[pari_path]),
    #"sagemath/dijkstra.pyx",
    Extension("sagemath.dijkstra",
              glob("sagemath/dijkstra.pyx"),
              extra_compile_args=["-std=c++17"],
              library_dirs=[pari_path]),
    #"sagemath/kssp.pyx",
    Extension("sagemath.kssp",
              glob("sagemath/kssp.pyx"),
              extra_compile_args=["-std=c++17"],
              library_dirs=[pari_path]),
    #"my_digraph.pyx"
    Extension("my_digraph",
              glob("my_digraph.pyx"),
              extra_compile_args=["-std=c++17"],
              library_dirs=[pari_path]),
              ]

setup(ext_modules=cythonize(extensions,
                            language_level=sys.version_info[0],  # 2 or 3
                            annotate=True,
                            compiler_directives=directives,
                           )
          )

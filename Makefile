

cpp:
	./build.sh

cython:
	export SAGE_ROOT="${HOME}/sage/"; \
	export DOT_SAGE="${HOME}/.sage/"; \
	${SAGE_ROOT}/sage setup.py build_ext --inplace

gelati:
	export SAGE_ROOT="/home/dcoudert/sage/"; \
	export DOT_SAGE="/home/dcoudert/.sage/"; \
	${SAGE_ROOT}/sage setup.py build_ext --inplace

clean:
	\rm -f sagemath/*.cpp sagemath/*.c sagemath/*.html sagemath/*.so
	\rm -f *.cpp *.so *.html
	\rm -rf build/
	\rm -rf */__pycache__

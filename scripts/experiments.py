"""
Experiments for JEA

We use Sagemath...
"""
"""
## This ensures that the next import requests will be in the correct directory
## i.e., we get the path to this file and we move to it
import inspect,os
working_directory = os.getcwd()
source_directory = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
os.chdir(source_directory)

load('./linear_ordering.py')
load('./graph-io.py')
load('./io-tools.py')

## Now that the code is loaded, we go back to the working directory
os.chdir(working_directory)
"""

from my_digraph import MyDiGraph


def choose_pairs_by_dijkstra_rank(D, nb_pairs=1000, nb_targets=100, reverse=True, out_filename=None):
    """
    Choose pairs of vertices by Dijkstra rank.

    We ensure that source and target of each pair are in the largest strongly
    connected component.

    INPUT:

    - ``in_filename`` -- filename storing a graph

    - ``out_filename`` -- filename where to store the result

    - ``nb_pairs`` -- total number of pairs to select

    - ``nb_targets`` -- number of targets to consider

    - ``reverse`` -- whether to consider shortest path tree from (in which case
      we select nb_targets sources) or to.
    """
    from sage.misc.prandom import shuffle

    # We build the largest strongly connected subgraph
    G = DiGraph(D.edges(), format='list_of_edges')
    if not G.is_strongly_connected():
        scc = max(G.strongly_connected_components(), key=len)
        G = G.subgraph(scc)

    # and search for a center of G
    # If G is large, it is way faster to take an undirected graph. The
    # computed center will be close to the center of the directed graph.
    if G.order() < 10000:
        center = G.center(by_weight=True).pop()
    else:
        H = Graph(G.edges(sort=False), format='list_of_edges')
        ecc = H.eccentricity(by_weight=True, algorithm='DHV', with_labels=True)
        r = min(ecc.values())
        for v, ec in ecc.items():
            if ec == r:
                center = v
                break
    dist_center = D.dijkstra(center, reverse=reverse)[1]

    n = G.order()
    pairs_per_source = nb_pairs/nb_targets
    first_rank = floor(n/(pairs_per_source + 1))
    step_rank = floor(n/pairs_per_source)
    
    V = list(G)
    shuffle(V)

    pairs = []
    for i, source in enumerate(V):
        if i >= nb_targets and len(pairs) >= nb_pairs:
            break
        # Compute Dijkstra rank from/to source
        dijkstra_rank = D.dijkstra_rank(source, reverse=reverse)
        # order vertices by increasing Dijkstra rank
        order = [k for k, v in sorted(dijkstra_rank.items(), key=lambda x:x[1]) if k in G]
        # compute distances
        distance = D.dijkstra(source, reverse=reverse)[1]
        # Select pairs
        for j in range(first_rank, n, step_rank):
            v = order[j]
            if reverse:
                pairs.append((v, source, dijkstra_rank[v], distance[v], (dist_center[v] + distance[center])/distance[v]))
            else:
                pairs.append((source, v, dijkstra_rank[v], distance[v], (distance[center] + dist_center[v])/distance[v]))

    if len(pairs) > nb_pairs:
        pairs = pairs[:nb_pairs]

    if out_filename is not None:
        with open(out_filename, 'w') as f:
            for p in pairs:
                f.write('{} {}\n'.format(p[0], p[1]))
        
        with open(out_filename + '_data', 'w') as f:
            f.write('# source\ttarget\tdijkstra rank\tdistance\tstretch ({})\treverse={}\n'.format(center, reverse))
            for p in pairs:
                f.write('\t'.join(map(str, p)) + '\n')
    
    return pairs



def choose_pairs_by_dijkstra_rank_log(D, nb_targets=100, reverse=True, out_filename=None):
    """
    Choose pairs of vertices by Dijkstra rank.

    We ensure that source and target of each pair are in the largest strongly
    connected component.

    INPUT:

    - ``in_filename`` -- filename storing a graph

    - ``out_filename`` -- filename where to store the result

    - ``nb_pairs`` -- total number of pairs to select

    - ``nb_targets`` -- number of targets to consider

    - ``reverse`` -- whether to consider shortest path tree from (in which case
      we select nb_targets sources) or to.
    """
    from sage.misc.prandom import shuffle

    # We build the largest strongly connected subgraph
    G = DiGraph(D.edges(), format='list_of_edges')
    if not G.is_strongly_connected():
        scc = max(G.strongly_connected_components(), key=len)
        G = G.subgraph(scc)

    # and search for a center of G
    # If G is large, it is way faster to take an undirected graph. The
    # computed center will be close to the center of the directed graph.
    if G.order() < 10000:
        center = G.center(by_weight=True).pop()
    else:
        H = Graph(G.edges(sort=False), format='list_of_edges')
        ecc = H.eccentricity(by_weight=True, algorithm='DHV', with_labels=True)
        r = min(ecc.values())
        for v, ec in ecc.items():
            if ec == r:
                center = v
                break
    dist_center = D.dijkstra(center, reverse=reverse)[1]

    n = G.order()
    ranks = [2]
    for exp in range(1, 81):
        j = 10**exp
        if j > n - 1:
            ranks.append(n - 1)
            break
        ranks.append(j)
    sranks = set(ranks)
    print(ranks)

    V = list(G)
    shuffle(V)

    pairs = []
    i = 0
    for source in V:
        if i >= nb_targets:
            break
        # Compute Dijkstra rank from/to source
        dijkstra_rank = D.dijkstra_rank(source, reverse=reverse)
        # Extract vertices to consider
        vertices = sorted([(r, k) for k, r in dijkstra_rank.items() if k in G and r in sranks])
        if len(vertices) < len(ranks):
            # avoid corner cases
            continue
        i += 1
        # compute distances
        distance = D.dijkstra(source, reverse=reverse)[1]
        # Select pairs
        for r, v in vertices:
            if reverse:
                pairs.append((v, source, r, distance[v], (dist_center[v] + distance[center])/distance[v]))
            else:
                pairs.append((source, v, r, distance[v], (distance[center] + dist_center[v])/distance[v]))

    if out_filename is not None:
        with open(out_filename, 'w') as f:
            for p in pairs:
                f.write('{} {}\n'.format(p[0], p[1]))

        with open(out_filename + '_data', 'w') as f:
            f.write('# source\ttarget\tdijkstra rank\tdistance\tstretch ({})\treverse={}\n'.format(center, reverse))
            for p in pairs:
                f.write('\t'.join(map(str, p)) + '\n')

    return pairs


def generate_grid_instances(path, X=10, M=10000):
    """
    Generate 2D grid instances
    """
    from sage.misc.prandom import randint

    for Y in range(X, 11*X, X):
        G = DiGraph(graphs.Grid2dGraph(X, Y))
        G.relabel(inplace=True)
        for u, v in G.edge_iterator(labels=False):
            G.set_edge_label(u, v, randint(0, M))

        name = 'G{}x{}.gr'.format(X, Y)
        with open(path + '/' + name, 'w') as f:
            f.write('c ----------------------------------------------------\n')
            f.write('c {}: {} x {} grid\n'.format(name, X, Y))
            f.write('c ----------------------------------------------------\n')
            f.write('p sp {} {}\n'.format(G.order(), G.size()))
            for u, v, l in G.edge_iterator():
                f.write('a {} {} {}\n'.format(u, v, l))

        pair_name = 'G{}x{}.pairs'.format(X, Y)
        with open(path + '/' + pair_name, 'w') as f:
            f.write('0 {}\n'.format(G.order() -1))

        center = G.center(by_weight=True).pop()
        D = MyDiGraph(G, by_weight=True)
        source = 0
        target = G.order() -1
        dijkstra_rank = D.dijkstra_rank(target, reverse=True)
        distance = D.dijkstra(target, reverse=True)[1]
        dist_center = D.dijkstra(center, reverse=True)[1]

        with open(path + '/' + pair_name + '_data', 'w') as f:
            f.write('# source\ttarget\tdijkstra rank\tdistance\tstretch ({})\treverse=True\n'.format(center))
            f.write('0\t{}\t{}\t{}\t{}\n'.format(target, dijkstra_rank[0], distance[0], (distance[center] + dist_center[source])/distance[source]))


def generate_grid_instances_v2(path, X=10, min_weight=0, max_weight=10000):
    """
    Generate 2D grid instances
    """
    from sage.misc.prandom import randint


    Y = 10 * X
    G = DiGraph(graphs.Grid2dGraph(X, Y))
    v_to_int = G.relabel(inplace=True, return_map=True)
    source = v_to_int[(0, 0)]
    target_1 = v_to_int[(0, 1)]
    target_Y = v_to_int[(0, Y - 1)]
    target = v_to_int[(X - 1, Y - 1)]
    for u, v in G.edge_iterator(labels=False):
        G.set_edge_label(u, v, randint(min_weight, max_weight))

    while Y >= X:
        # Write instance
        name = 'G2_{}x{}.gr'.format(X, Y)
        with open(path + '/' + name, 'w') as f:
            f.write('c ----------------------------------------------------\n')
            f.write('c {}: {} x {} grid\n'.format(name, X, Y))
            f.write('c ----------------------------------------------------\n')
            f.write('p sp {} {}\n'.format(G.order(), G.size()))
            for u, v, l in G.edge_iterator():
                f.write('a {} {} {}\n'.format(u, v, l))

        pair_name = 'G2_{}x{}.pairs'.format(X, Y)
        with open(path + '/' + pair_name, 'w') as f:
            f.write('{} {}\n'.format(source, target_1))
            f.write('{} {}\n'.format(source, target_Y))
            f.write('{} {}\n'.format(source, target))

        # It's too long to find a center, so we pick any node.
        # center = G.center(by_weight=True).pop()
        center = v_to_int[(X//2, Y//2)]
        D = MyDiGraph(G, by_weight=True)
        dijkstra_rank = D.dijkstra_rank(target, reverse=True)
        distance = D.dijkstra(target, reverse=True)[1]
        dijkstra_rank_1 = D.dijkstra_rank(target_1, reverse=True)
        distance_1 = D.dijkstra(target_1, reverse=True)[1]
        dijkstra_rank_Y = D.dijkstra_rank(target_Y, reverse=True)
        distance_Y = D.dijkstra(target_Y, reverse=True)[1]
        dist_center = D.dijkstra(center, reverse=True)[1]

        with open(path + '/' + pair_name + '_data', 'w') as f:
            f.write('# source\ttarget\tdijkstra rank\tdistance\tstretch ({})\treverse=True\n'.format(center))
            f.write('{}\t{}\t{}\t{}\t{}\n'.format(source, target_1, dijkstra_rank_1[source], distance_1[source],
                                                  (distance_1[center] + dist_center[source])/distance_1[source]))
            f.write('{}\t{}\t{}\t{}\t{}\n'.format(source, target_Y, dijkstra_rank_Y[source], distance_Y[source],
                                                  (distance_Y[center] + dist_center[source])/distance_Y[source]))
            f.write('{}\t{}\t{}\t{}\t{}\n'.format(source, target, dijkstra_rank[source], distance[source],
                                                  (distance[center] + dist_center[source])/distance[source]))

        if X == Y:
            break

        # remove the last X columns, remove the entries from v_to_int and update target
        Y -= X
        to_remove = [u for u in v_to_int.keys() if u[1] >= Y]
        G.delete_vertices(v_to_int.pop(u) for u in to_remove)
        target = v_to_int[(X - 1, Y - 1)]
        target_Y = v_to_int[(0, Y - 1)]





networks = {
    'Rome': 'graphs/Road-networks/Rome.gr',
    'DC': 'graphs/Road-networks/DC.gr',
    'DE': 'graphs/Road-networks/DE.gr',
    'NY': 'graphs/Road-networks/NY.gr',
    'BAY': 'graphs/Road-networks/BAY.gr',
    'COL': 'graphs/Road-networks/COL.gr',
    'FB': 'graphs/Complex-networks/FB.gr',
    'P2P': 'graphs/Complex-networks/P2P.gr',
    'DIP': 'graphs/Complex-networks/DIP.gr',
    'LOC': 'graphs/Complex-networks/LOC.gr',
    'CAIDA': 'graphs/Complex-networks/CAIDA.gr',
    'BIOGRID': 'graphs/Complex-networks/BIOGRID.gr', 
    }

grids = {
    'G10x10': 'graphs/grids/G10x10.gr',
    'G10x20': 'graphs/grids/G10x20.gr',
    'G10x30': 'graphs/grids/G10x30.gr',
    'G10x40': 'graphs/grids/G10x40.gr',
    'G10x50': 'graphs/grids/G10x50.gr',
    'G10x60': 'graphs/grids/G10x60.gr',
    'G10x70': 'graphs/grids/G10x70.gr',
    'G10x80': 'graphs/grids/G10x80.gr',
    'G10x90': 'graphs/grids/G10x90.gr',
    'G10x100': 'graphs/grids/G10x100.gr',
    }

def run_experiments(data, output_dir, pairs=True, bylog=True, run=True):
    """
    Run some experiments
    """
    import os
    working_directory = os.getcwd()

    for name, path in data.items():
        out_file = output_dir + '/' + name + '.pairs'

        if pairs:
            D = MyDiGraph(path, by_weight=True)
            print("generate pairs, stored in ", out_file)
            if bylog:
                _ = choose_pairs_by_dijkstra_rank_log(D, out_filename=out_file)
            else:
                _ = choose_pairs_by_dijkstra_rank(D, out_filename=out_file)

        if not run:
            continue

        cmd = './build/experiments'
        cmd += ' ./' + path
        cmd += ' ./' + out_file
        if name in ['Rome', 'DC', 'DE']  or name.startswith('G10x'):
            cmd += ' -y'
        elif name in ['BIOGRID', 'CAIDA', 'FB', 'P2P', 'DIP', 'LOC']:
            cmd += ' -B'
        #else:
        #    cmd += ' 5'
        cmd += ' > {}/{}.out'.format(output_dir, name)
        print("run command: ", cmd)
        os.system(cmd)




def run_experiments_grids(output_dir):
    """
    Run some experiments
    """
    import os
    working_directory = os.getcwd()

    for name, path in grids.items():

        cmd = './build/experiments 3 ./' + path
        cmd += ' ./' + path.replace('.gr', '.pairs')
        cmd += ' > {}/{}.out'.format(output_dir, name)
        print("run command: ", cmd)
        os.system(cmd)

    
def run_experiments_grids_v2(input_dir, output_dir):
    """
    Run some experiments
    """
    import os
    working_directory = os.getcwd()

    for name in os.listdir(input_dir):
        if not name.startswith('G2_') or not name.endswith('.gr'):
            continue
        path = input_dir + '/' + name
        cmd = './build/experiments ./' + path
        cmd += ' ./' + path.replace('.gr', '.pairs')
        cmd += ' -B'  # for 10k paths
        cmd += ' > {}/{}.out'.format(output_dir, name.replace('.gr', ''))
        print("run command: ", cmd)
        os.system(cmd)

    

"""
For each network XX, we have 3 files:
- XX.pairs: list of source-destination pairs

- XX.pairs_data: file containing for each pair source-destination
  - source
  - target
  - Dijkstra rank
  - shortest path distance
  - stretch of the shortest path with respect a central node (the same for all pairs)

- XX.out: 
  - algorithm
  - source
  - target
  - shortest path distance
  - hop distance
  - ???
  - k
  - k*
  - number of stored or computed trees
  - running time (ms)
"""

int_to_algorithm = ['Y', 'NC', 'PNC', 'PNC*', 'SB', 'SB*', 'PSB', 'PSBv2', 'PSBv3']
#int_to_algorithm = ['Y', 'NC', 'PNC', 'SB', 'SB*', 'PSB', 'PSBv2', 'PSBv3']
algorithm_to_int = {algo: i for i, algo in enumerate(int_to_algorithm)}

networks = {
    'Rome': 'graphs/Road-networks/Rome.gr',
    'DC': 'graphs/Road-networks/DC.gr',
    'DE': 'graphs/Road-networks/DE.gr',
    'NY': 'graphs/Road-networks/NY.gr',
    'BAY': 'graphs/Road-networks/BAY.gr',
    'COL': 'graphs/Road-networks/COL.gr',
    'FB': 'graphs/Complex-networks/FB.gr',
    'P2P': 'graphs/Complex-networks/P2P.gr',
    'DIP': 'graphs/Complex-networks/DIP.gr',
    'LOC': 'graphs/Complex-networks/LOC.gr',
    'CAIDA': 'graphs/Complex-networks/CAIDA.gr',
    'BIOGRID': 'graphs/Complex-networks/BIOGRID.gr', 
    }

cities = ['Rome', 'DC', 'DE', 'NY', 'BAY', 'COL']
complex_networks = ['BIOGRID', 'FB', 'P2P', 'DIP', 'CAIDA', 'LOC']

    
grids = {
    'G10x10': 'graphs/grids/G10x10.gr',
    'G10x20': 'graphs/grids/G10x20.gr',
    'G10x30': 'graphs/grids/G10x30.gr',
    'G10x40': 'graphs/grids/G10x40.gr',
    'G10x50': 'graphs/grids/G10x50.gr',
    'G10x60': 'graphs/grids/G10x60.gr',
    'G10x70': 'graphs/grids/G10x70.gr',
    'G10x80': 'graphs/grids/G10x80.gr',
    'G10x90': 'graphs/grids/G10x90.gr',
    'G10x100': 'graphs/grids/G10x100.gr',
    }

algo_color = {
    'Y': 'black',
    'PY': 'orange',
    'NC': 'gray',
    'PNC': 'red',
    'PNC*': 'orange',
    'SB': 'blue',
    'SB*': 'green',
    'PSB': 'brown',
    'PSBv2': 'purple',
    'PSBv3': 'pink',
    }
algo_marker = {
    'Y': 'o',
    'PY': 'p',
    'NC': 's',
    'PNC': 'p',  # or 'x',
    'PNC*': 'p',
    'SB': 'D',
    'SB*': 'd',
    'PSB': 6,  # or 'h',
    'PSBv2': '<',
    'PSBv3': '>',
    }




def load_pairs_data(filename):
    """
    Load pairs data stored in filename

    Return a dictionary associating to each pair (source, target) a list of 3
    values: dijkstra rank, shortest path distance and stretch of shortest path
    with respect a central vertex of the graph
    """
    data = {}
    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('#'):
                continue
            s, t, r, d, st = line.split()
            data[ZZ(s), ZZ(t)] = [ZZ(r), ZZ(d), RR(st)]

    return data



def load_values_for_fixed_k(filename, k=1000):
    """
    Return:
    - an ordered list of pairs
    - a dictionary associating to each algorithm the running time for kSSSP. The
      values are ordered in the same order as the pairs
    - a dictionary associating to each algorithm the number of stored trees or
      number of Dikjstra run for kSSSP. The values are ordered in the same order
      as the pairs
    """
    time = {algo: {} for algo in int_to_algorithm}
    trees = {algo: {} for algo in int_to_algorithm}

    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('#') or line.startswith('Algo') or len(line) < 3:
                continue
            # We split the line according the different columns
            algo, s, t, dist, hop, Mhop, kk, kks, tr, tt = line.split()
            if ZZ(kk) != k:
                continue
            p = (ZZ(s), ZZ(t))
            time[algo][p] = ZZ(tt)
            trees[algo][p] = ZZ(tr)

    # We first extract an ordered list of pairs
    for algo in time:
        if time[algo]:
            pairs = sorted(time[algo])
            break

    pair_to_int = {p: i for i, p in enumerate(pairs)}
    
    # We now simplify the lists, keeping only time or # tree
    # We ensure that the lists are sorted in the same order of pairs
    for algo in int_to_algorithm:
        tmp = time[algo]
        if tmp:
            time[algo] = [tmp[p] for p in pairs]
        tmp = trees[algo]
        if tmp:
            trees[algo] = [tmp[p] for p in pairs]

    return pairs, time, trees


def load_values_for_increasing_k(filename):
    """
    Return
    - an ordered list of used values of k
    - a dictionary associating to each algorithm a dictionary containing
      - the mean of the running times for each value of k (in the right order)
      - the median of the running times for each value of k (in the right order)
      - the maximum of the running times for each value of k (in the right order)
    - a similar dictionary for trees
    """
    time = {algo: {} for algo in int_to_algorithm}
    trees = {algo: {} for algo in int_to_algorithm}

    with open(filename, 'r') as f:
        for line in f:
            if line.startswith('#') or line.startswith('Algo') or len(line) < 3:
                continue
            # We split the line according the different columns
            algo, s, t, dist, hop, Mhop, kk, kks, tr, tt = line.split()
            k = ZZ(kk)
            if k not in time[algo]:
                time[algo][k] = [ZZ(tt)]
                trees[algo][k] = [ZZ(tr)]
            else:
                time[algo][k].append(ZZ(tt))
                trees[algo][k].append(ZZ(tr))

    # We first extract the list of k values
    for algo in time:
        if time[algo]:
            k_values = sorted(time[algo])
            break

    import numpy

    for algo in time:
        tmp = time[algo]
        if not tmp:
            continue
        time[algo] = {'mean': [max(10**(-6), numpy.mean(tmp[k])) for k in k_values],
                      'median': [max(10**(-6), numpy.median(tmp[k])) for k in k_values],
                      'max': [max(tmp[k]) for k in k_values]}
        tmp = trees[algo]
        trees[algo] = {'mean': [max(10**(-6), numpy.mean(tmp[k])) for k in k_values],
                       'median': [max(10**(-6), numpy.median(tmp[k])) for k in k_values],
                       'max': [max(tmp[k]) for k in k_values]}

    return k_values, time, trees



def scatter_plot_algo_algo(source_dir, target_dir, name, k=1000):
    """
    Plot a certain number of scatter plots to compare the running time / # trees
    of some algorithms.

    The plots are stored in files target_dir/name_time_algo1_algo.png
    """
    pairs, time, trees = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)

    AB = [('SB', 'SB*'), ('NC', 'PNC'), ('SB*', 'PNC'), ('SB', 'PSB')]  # ('PNC', 'PNC*'),

    base_name = target_dir + '/' + name
    star = lambda s:s.replace('*', 's')
    for a, b in AB:
        P = list_plot(list(zip(time[a], time[b])),
                      legend_label = '$(x,y) = ({}, {})$'.format(a, b),
                      axes_labels=['Running time of {} (ms)'.format(a),
                                   'Running time of {} (ms)'.format(b)],
                      axes_labels_size=1.6)
        x = var('x')
        P += plot(x, (x, 0, max(max(time[a]), max(time[b]))), frame=True,
                  legend_label='$y = x$', color='green')
        P.save_image(base_name + '_time_{}_{}.png'.format(star(a), star(b)))


    for a, b in [('SB', 'PSB')]:
        P = list_plot(list(zip(trees[a], trees[b])),
                      legend_label = '$(x,y) = ({}, {})$'.format(a, b),
                      axes_labels=['Number of stored trees of {}'.format(a),
                                   'Number of stored trees of {}'.format(b)],
                      axes_labels_size=1.6)
        x = var('x')
        P += plot(x, (x, 0, max(max(trees[a]), max(trees[b]))), frame=True,
                  legend_label='$y = x$', color='green')
        P.save_image(base_name + '_trees_{}_{}.png'.format(star(a), star(b)))


def scatter_plot_algo_algo_rainbow(source_dir, target_dir, name, k=1000, rainbow=True):
    """
    Plot a certain number of scatter plots to compare the running time / # trees
    of some algorithms.

    The plots are stored in files target_dir/name_time_algo1_algo.png
    """
    import matplotlib.mlab as mlab
    import matplotlib.pyplot as plt

    pairs, time, trees = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)
    pairs_data = load_pairs_data(source_dir + '/' + name + '.pairs_data')

    dijkstra_rank = [pairs_data[p][0] for p in pairs]
    ranks = sorted(set(dijkstra_rank))

    AB = [('SB', 'SB*'), ('NC', 'PNC'), ('SB*', 'PNC'), ('SB', 'PSB')]  # ('PNC', 'PNC*'),

    base_name = target_dir + '/' + name
    star = lambda s:s.replace('*', 's')

    tick_size = 10
    label_size = 14

    for a, b in AB:
        # Clear the figure
        plt.clf()

        # plot xy
        xy_max = max(max(time[a]), max(time[b]))
        plt.plot([0, xy_max], [0, xy_max], color='black', label='$y = x$')

        # plot the points
        if rainbow:
            sc = plt.scatter(time[a], time[b], c=dijkstra_rank, marker='o', s=12,
                             linewidths=0,label='$(x,y) = ({}, {})$'.format(a, b))
            # add color bar
            sca = plt.colorbar(aspect=30, pad=0.02, location='right')
            # sca.ticklabel_format(style='sci', scilimits=(0, 0))
            sca.set_label('Dijkstra rank', fontsize=label_size)
        else:
            sc = plt.scatter(time[a], time[b], marker='o', s=12,
                             linewidths=0,label='$(x,y) = ({}, {})$'.format(a, b))

        plt.rc('xtick', labelsize=tick_size)
        plt.rc('ytick', labelsize=tick_size)
        plt.xlim([0, xy_max*1.001])
        plt.ylim([0, xy_max*1.001])
        #plt.ticklabel_format(style='sci', axis='x', scilimits=(0, 0))
        #plt.ticklabel_format(style='sci', axis='y', scilimits=(0, 0))

        # set axis
        plt.xlabel('Running time of {} (ms)'.format(a), fontsize=label_size)
        plt.ylabel('Running time of {} (ms)'.format(b), fontsize=label_size)

        # Tweak spacing to maximize fig size
        #plt.subplots_adjust(left=0.2, right=1.04, bottom=0.08,top=0.98)
        plt.tight_layout()

        plt.legend(loc=0, fontsize=label_size, frameon=True)
        plt.savefig(base_name + '_time_{}_{}{}.png'.format(star(a), star(b), '_rainbow' if rainbow else ''))


    for a, b in [('SB', 'PSB')]:
        # Clear the figure
        plt.clf()

        # plot xy
        xy_max = max(max(trees[a]), max(trees[b]))
        plt.plot([0, xy_max], [0, xy_max], color='black', label='$y = x$')

        # plot the points
        if rainbow:
            sc = plt.scatter(trees[a], trees[b], c=dijkstra_rank, marker='o', s=12,
                             linewidths=0,label='$(x,y) = ({}, {})$'.format(a, b))
            # add color bar
            sca = plt.colorbar(aspect=30, pad=0.02, location='right')
            sca.set_label('Dijkstra rank', fontsize=label_size)
        else:
            sc = plt.scatter(trees[a], trees[b], marker='o', s=12,
                             linewidths=0,label='$(x,y) = ({}, {})$'.format(a, b))


        plt.rc('xtick', labelsize=tick_size)
        plt.rc('ytick', labelsize=tick_size)
        plt.xlim([0, xy_max*1.001])
        plt.ylim([0, xy_max*1.001])

        # set axis
        plt.xlabel('Number of stored trees of {}'.format(a), fontsize=label_size)
        plt.ylabel('Number of stored trees of {}'.format(b), fontsize=label_size)

        # Tweak spacing to maximize fig size
        #plt.subplots_adjust(left=(0.09 if log_scale else 0.08), right=1.04, bottom=0.08,top=0.98)
        plt.tight_layout()

        plt.legend(loc=0, fontsize=label_size, frameon=True)
        plt.savefig(base_name + '_trees_{}_{}{}.png'.format(star(a), star(b), '_rainbow' if rainbow else ''))



def scatter_plot_algo_algo_rainbow_nico(source_dir, target_dir, name, k=1000, rainbow=True):
    """
    Plot a certain number of scatter plots to compare the running time / # trees
    of some algorithms.

    The plots are stored in files target_dir/name_time_algo1_algo.png
    """
    import matplotlib.mlab as mlab
    import matplotlib.pyplot as plt

    pairs, time, trees = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)
    pairs_data = load_pairs_data(source_dir + '/' + name + '.pairs_data')

    dijkstra_rank = [pairs_data[p][0] for p in pairs]
    ranks = sorted(set(dijkstra_rank))

    stats = {r: {'PNC': 0, 'SB*': 0} for r in ranks}

    for a, b, r in zip(time['SB*'], time['PNC'], dijkstra_rank):
        if a < b:
            stats[r]['SB*'] += 1
        else:
            stats[r]['PNC'] += 1

    for r in ranks:
        print(r, stats[r])

    A = "Dijkstra rank"
    B = "SB*"
    C = "PNC"
    for r in ranks:
        A += " & " + str(r)
        B += " & " + str(stats[r]['SB*'])
        C += " & " + str(stats[r]['PNC'])

    print(A + "\\\\\\hline")
    print(B + "\\\\\\hline")
    print(C + "\\\\\\hline")


def scatter_plot_time_per_rank_or_stretch(source_dir, target_dir, name, k=1000):
    """
    Scatter plot rank/stretch versus running time of some algorithms.

    The rank is the Dijkstra rank.

    The stretch is the ratio of the source-target distance via a central node
    over the shortest source-target distance.
    """
    pairs, time, trees = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)
    pairs_data = load_pairs_data(source_dir + '/' + name + '.pairs_data')

    base_name = target_dir + '/' + name
    star = lambda s:s.replace('*', 's')
    dijkstra_rank = [pairs_data[p][0] for p in pairs]

    for algo in int_to_algorithm:
        if not time[algo]:
            continue
        P = list_plot(list(zip(dijkstra_rank, time[algo])),
                      #legend_label = '$(x,y) = ({}, {})$'.format(a, b),
                      axes_labels=['Dijkstra rank',
                                   'Running time of {} (ms)'.format(algo)],
                      axes_labels_size=1.6, frame=True)
        P.save_image(base_name + '_rank_time_{}.png'.format(star(algo)))

        stretch = [pairs_data[p][2] for p in pairs]
        P = list_plot(list(zip(stretch, time[algo])),
                      #legend_label = '$(x,y) = ({}, {})$'.format(a, b),
                      axes_labels=['Stretch value',
                                   'Running time of {} (ms)'.format(algo)],
                      axes_labels_size=1.6, frame=True)
        P.save_image(base_name + '_stretch_time_{}.png'.format(star(algo)))

        if 'NC' in algo:
            continue
        P = list_plot(list(zip(dijkstra_rank, trees[algo])),
                      #legend_label = '$(x,y) = ({}, {})$'.format(a, b),
                      axes_labels=['Dijkstra rank',
                                   'Number of stored trees ({})'.format(algo)],
                      axes_labels_size=1.6, frame=True)
        P.save_image(base_name + '_rank_trees_{}.png'.format(star(algo)))

        stretch = [pairs_data[p][2] for p in pairs]
        P = list_plot(list(zip(stretch, trees[algo])),
                      #legend_label = '$(x,y) = ({}, {})$'.format(a, b),
                      axes_labels=['Stretch value',
                                   'Number of stored trees ({})'.format(algo)],
                      axes_labels_size=1.6, frame=True)
        P.save_image(base_name + '_stretch_trees_{}.png'.format(star(algo)))
        


def box_plot_time_per_rank(source_dir, target_dir, name, k=1000):
    """
    Box plot rank/stretch versus running time of some algorithms.

    The rank is the Dijkstra rank.

    The stretch is the ratio of the source-target distance via a central node
    over the shortest source-target distance.
    """
    pairs, time, _ = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)
    pairs_data = load_pairs_data(source_dir + '/' + name + '.pairs_data')

    base_name = target_dir + '/' + name
    star = lambda s:s.replace('*', 's')
    dijkstra_rank = [pairs_data[p][0] for p in pairs]
    ranks = sorted(set(dijkstra_rank))

    for algo in int_to_algorithm:
        if not time[algo]:
            continue
        data = {rank: [] for rank in ranks}
        for r, t in zip(dijkstra_rank, time[algo]):
            data[r].append(t)

        from matplotlib import pyplot as plt
        fig, ax = plt.subplots(figsize=(9, 4))
        bp = ax.boxplot([data[rank] for rank in ranks],
                        vert=True,
                        #patch_artist=True,
                        labels=ranks,
                        widths=.2)
        #for patch, color in zip(bp['boxes'], colors):
        #    patch.set_facecolor(color)
        ax.set_xlabel('Dijkstra rank', fontsize=16)
        ax.set_ylabel('Running time of {} (ms)'.format(algo), fontsize=16)
        plt.savefig(base_name + '_rank_box_time_{}.png'.format(star(algo)))
        plt.close()



def plot_evolution_time_tree_per_rank(source_dir, target_dir, name):
    """
    One plot per algorithm:
    - 1 curve per rank
    - k vs running time or k versus trees

    # NOT INTERESTING....
    """
    import numpy
    pairs_data = load_pairs_data(source_dir + '/' + name + '.pairs_data')
    dijkstra_rank = [pairs_data[p][0] for p in pairs_data]
    ranks = sorted(set(dijkstra_rank))

    k_values, _, _ = load_values_for_increasing_k(source_dir + '/' + name + '.out')
    data_time = {}
    data_trees = {}
    for k in k_values:
        pairs, tmp_time, tmp_trees = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)
        data_time[k] = tmp_time
        data_trees[k] = tmp_trees

    base_name = target_dir + '/' + name
    star = lambda s:s.replace('*', 's')

    for algo in int_to_algorithm:
        if algo == 'PNC*' or not data_time[k_values[0]][algo]:
            continue

        # Format data
        D = {rank: {k: [] for k in k_values} for rank in ranks}
        M = 0
        for k in k_values:
            for r, t in zip(dijkstra_rank, data_time[k][algo]):
                D[r][k].append(t)
                M = max(M, t)

        P = list_plot_loglog([(k_values[0], M)], color='white')
        for rank in ranks:
            tmp = [numpy.median(D[rank][k]) for k in k_values]
            P += list_plot_loglog(list(zip(k_values, tmp)),
                                  #color=algo_color[algo],
                                  plotjoined=True,
                                  linestyle='-.',
                                  #marker=algo_marker[algo],
                                  markersize=3,
                                  markeredgewidth=5,
                                  thickness=1,
                                  legend_label='  ' + str(rank),
                                  figsize=[6,4],
                                  ticks=[k_values, None],
                                  tick_formatter=[k_values, None],
                                  axes_labels=['$k$', 'time (ms)'],
                                  axes_labels_size=1.5)
        P.save_image(base_name + '_evolution_time_per_rank_{}.png'.format(algo))
            

def plot_evolution_time_per_rank(source_dir, target_dir, name, k=1000):
    """
    One line per algorithm.
    x: Mean/median per Dijkstra rank
    y: running time
    """
    import numpy
    pairs, time, trees = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)
    pairs_data = load_pairs_data(source_dir + '/' + name + '.pairs_data')

    base_name = target_dir + '/' + name
    star = lambda s:s.replace('*', 's')
    dijkstra_rank = [pairs_data[p][0] for p in pairs]
    ranks = sorted(set(dijkstra_rank))

    good_time = {}
    m, M = +oo, 0
    for algo in int_to_algorithm:
        if algo == 'PNC*' or not time[algo]:
            continue
        tmp = {rank: [] for rank in ranks}
        for r, t in zip(dijkstra_rank, time[algo]):
            tmp[r].append(t)
        avg = [numpy.mean(tmp[rank]) for rank in ranks]
        med = [numpy.median(tmp[rank]) for rank in ranks]
        good_time[algo] = {'mean': avg, 'median': med}
        m = min(m, min(avg))
        m = min(m, min(med))
        M = max(M, max(avg))
        M = max(M, max(med))

    xx = {'mean': 'Average', 'median': 'Median of'}
    for op in ['mean', 'median']:
        P = list_plot_semilogy([])
        for algo in int_to_algorithm:
            if algo == 'PNC*' or not time[algo]:
                continue
            P += list_plot_semilogy(list(zip(ranks, good_time[algo][op])),
                                    color=algo_color[algo],
                                    plotjoined=True,
                                    linestyle='-.',
                                    marker=algo_marker[algo],
                                    markersize=3,
                                    markeredgewidth=5,
                                    thickness=1,
                                    legend_label='  ' + algo)
        #print(m, M)
        P += list_plot_semilogy([(ranks[0], max(10, m - 50)), (ranks[-1], M + 50)], color='white',
                                frame=True,
                                figsize=[8,4],
                                ticks=[ranks, None],
                                tick_formatter=[ranks, None],
                                axes_labels=['Dijkstra rank', '{} running time (ms)'.format(xx[op])],
                                axes_labels_size=1.5)
        P.set_legend_options(loc=3) #{'loc': 'lower left'})
        P.save_image(base_name + '_evolution_{}_time_per_rank.png'.format(op))

def plot_evolution_trees_per_rank(source_dir, target_dir, name, k=1000, calls=False):
    """
    One line per algorithm.
    x: Mean/median per Dijkstra rank
    y: running time# trees
    """
    import numpy
    pairs, time, trees = load_values_for_fixed_k(source_dir + '/' + name + '.out', k=k)
    pairs_data = load_pairs_data(source_dir + '/' + name + '.pairs_data')

    base_name = target_dir + '/' + name
    star = lambda s:s.replace('*', 's')
    dijkstra_rank = [pairs_data[p][0] for p in pairs]
    ranks = sorted(set(dijkstra_rank))

    good_time = {}
    m, M = +oo, 0
    for algo in int_to_algorithm:
        if algo == 'PNC*' or not trees[algo]:
            continue
        tmp = {rank: [] for rank in ranks}
        for r, t in zip(dijkstra_rank, trees[algo]):
            if not calls and algo == 'Y':
                tmp[r].append(1 - 10**(-6))
            elif not calls and 'NC' in algo:
                tmp[r].append(1)
            else:
                tmp[r].append(t)
        avg = [numpy.mean(tmp[rank]) for rank in ranks]
        med = [numpy.median(tmp[rank]) for rank in ranks]
        good_time[algo] = {'mean': avg, 'median': med}
        m = min(m, min(avg))
        m = min(m, min(med))
        M = max(M, max(avg))
        M = max(M, max(med))

    xx = {'mean': 'Average', 'median': 'Median of'}
    for op in ['mean', 'median']:
        P = list_plot_semilogy([])
        for algo in int_to_algorithm:
            if algo == 'PNC*' or not trees[algo]:
                continue
            P += list_plot_semilogy(list(zip(ranks, good_time[algo][op])),
                                    color=algo_color[algo],
                                    plotjoined=True,
                                    linestyle='-.',
                                    marker=algo_marker[algo],
                                    markersize=3,
                                    markeredgewidth=5,
                                    thickness=1,
                                    legend_label='  ' + algo)
        #print(m, M)
        if calls:
            y_name = '{} number of Dijkstra calls'.format(xx[op])
        else:
            y_name = '{} number of stored trees'.format(xx[op])
        P += list_plot_semilogy([(ranks[0], max(10, m - 50)), (ranks[-1], M + 50)], color='white',
                                frame=True,
                                figsize=[8,4],
                                ticks=[ranks, None],
                                tick_formatter=[ranks, None],
                                axes_labels=['Dijkstra rank', y_name],
                                axes_labels_size=1.5)
        P.set_legend_options(loc=3) #{'loc': 'lower left'})
        if calls:
            P.save_image(base_name + '_evolution_{}_dijkstra_per_rank.png'.format(op))
        else:
            P.save_image(base_name + '_evolution_{}_tree_per_rank.png'.format(op))

def plot_evolution_running_time_per_k(source_dir, target_dir, name):
    """
    """
    k_values, time, trees = load_values_for_increasing_k(source_dir + '/' + name + '.out')

    # value used to ensure the 2 figures have the same scale
    M = max([max(time[algo][value]) for algo in int_to_algorithm for value in ['mean', 'median']
                 if time[algo]])
    
    base_name = target_dir + '/' + name
    for value in ['mean', 'median']:
        P = list_plot_loglog([(k_values[0], M)], color='white')
        for algo in int_to_algorithm:
            if algo == 'PNC*' or not time[algo]:
                continue
            if any(t <= 0 for t  in time[algo][value]):
                print(algo, value, time[algo][value])
            P += list_plot_loglog(list(zip(k_values, time[algo][value])),
                                  color=algo_color[algo],
                                  plotjoined=True,
                                  linestyle='-.',
                                  marker=algo_marker[algo],
                                  markersize=3,
                                  markeredgewidth=5,
                                  thickness=1,
                                  legend_label='  ' + algo,
                                  figsize=[6,4],
                                  ticks=[k_values, None],
                                  tick_formatter=[k_values, None],
                                  axes_labels=['$k$', 'time (ms)'],
                                  axes_labels_size=1.5)
        P.set_legend_options(loc=4 if name == 'BIOGRID' else 0)
        P.save_image(base_name + '_time_{}.png'.format(value))


        
            
def table_cities(source_dir, target_dir):
    """
    TO BE TESTED !!!!
    """
    data_time = {}
    data_tree = {}
    for city in cities:
        _, time, trees = load_values_for_increasing_k(source_dir + '/' + city + '.out')
        data_time[city] = time
        data_tree[city] = trees

    k = 1000

    data = {'mean': {city: [] for city in cities},
            'median': {city: [] for city in cities},
            'max': {city: [] for city in cities}}

    for algo in int_to_algorithm:
        for val in ['mean', 'median', 'max']:
            for city in cities:
                if data_time[city][algo]:
                    #print(data_time[city][algo])
                    data[val][city].append(Integer(round(data_time[city][algo][val][-1], 0)))
                else:
                    data[val][city].append(+oo)

    def my_str(v):
        z = 0
        st = str(v)
        if len(st) > 3:
            if len(st) % 3:
                z = 3 - len(st)%3
                st = st.zfill(len(st) + z)
            st = '\\,'.join(st[i:i+3] for i in range(0, len(st), 3))
        return st[z:]

    def to_str(v, best):
        if v == +oo:
            return '-'
        if v == best:
            return '\\textbf{' + my_str(v) + '}'
        return my_str(v)

    best = {val: {} for val in data}
    for val in data:
        for city in data[val]:
            best[val][city] = min(data[val][city])
    for val in data:
        for city in data[val]:
            data[val][city] = [to_str(v, best[val][city]) for v in data[val][city]]


    s = '\\begin{table}[htbp]\n'
    s += '\\centering\n'
    s += '\\begin{tabular}{|l|c||r|r|r|r|r|r|r|}\n'
    s += '\\hline\n'
    s += '                    	&     & \\multicolumn{1}{c|}{Rome} & \\multicolumn{1}{c|}{DC}	& \\multicolumn{1}{c|}{DE} & \\multicolumn{1}{c|}{NY} & \\multicolumn{1}{c|}{BAY} & \\multicolumn{1}{c|}{COL}\\\\ \\hline\\hline\n'

    for i, algo in enumerate(int_to_algorithm):
        s += '\\multirow{3}{*}{' + algo + '}\t& avg & '
        s += ' & '.join([data['mean'][city][i] for city in cities])
        s += '\\\\ \\cline{2-8}\n'
        s += '                    	& med & '
        s += ' & '.join([data['median'][city][i] for city in cities])
        s += '\\\\ \\cline{2-8}\n'
        s += '                    	& max & '
        s += ' & '.join([data['max'][city][i] for city in cities])
        s += '\\\\\\hline\n\n'
                
    s += '\\end{tabular}\n'
    s += '\\caption{Running time (ms) of the algorithms on road networks,  ($k=1,000$)}\n'
    s += '\\label{tab:RunRoad}\n'
    s += '\\end{table}\n'

    with open(target_dir + '/' + 'table_cities_time.tex', 'w') as f:
        f.write(s)

    # TO DO table pour trees !!!!!
    data = {}
    best = {}
    for city in cities:
        data[city] = {algo: Integer(round(data_tree[city][algo]['mean'][-1], 0)) for algo in ['SB', 'PSB', 'PSBv2', 'PSBv3']}
        best[city] = min(data[city].values())
    for city in cities:
        data[city] = {algo: to_str(v, best[city]) for algo, v in data[city].items()}

    s = '\\begin{table}[htbp]\n'
    s += '\\centering\n'
    s += '\\begin{tabular}{|l||r|r|r|r|r|r|r|}\n'
    s += '\\hline\n'
    s += '                  & \\multicolumn{1}{c|}{Rome} & \\multicolumn{1}{c|}{DC}	& \\multicolumn{1}{c|}{DE} & \\multicolumn{1}{c|}{NY} & \\multicolumn{1}{c|}{BAY} & \\multicolumn{1}{c|}{COL}\\\\ \\hline\\hline\n'
    s += 'NC, PNC and PNC*  & 1     & 1     & 1     & 1     & 1     & 1\\\\ \\hline\n'
    s += 'SB and SB*        & '
    s += ' & '.join([data[city]['SB'] for city in cities])
    s += '\\\\ \\hline\n'
    s += 'PSB               & '
    s += ' & '.join([data[city]['PSB'] for city in cities])
    s += '\\\\ \\hline\n'
    s += 'PSBv2             & '
    s += ' & '.join([data[city]['PSBv2'] for city in cities])
    s += '\\\\ \\hline\n'
    s += 'PSBv3             & '
    s += ' & '.join([data[city]['PSBv3'] for city in cities])
    s += '\\\\ \\hline\n'                
    s += '\\end{tabular}\n'
    s += '\\caption{Average number of stored trees using some $kSSP$ algorithms on road networks,  ($k=1,000$)}\n'
    s += '\\label{tab:in-branching-road}\n'
    s += '\\end{table}\n'

    with open(target_dir + '/' + 'table_cities_trees.tex', 'w') as f:
        f.write(s)


def table_complex(source_dir, target_dir):
    """
    TO BE TESTED !!!!
    """
    data_time = {}
    data_tree = {}
    for name in complex_networks:
        _, time, trees = load_values_for_increasing_k(source_dir + '/' + name + '.out')
        data_time[name] = time
        data_tree[name] = trees

    k = 10000

    data = {'mean': {name: [] for name in complex_networks},
            'median': {name: [] for name in complex_networks},
            'max': {name: [] for name in complex_networks}}

    for algo in int_to_algorithm:
        for val in ['mean', 'median', 'max']:
            for name in complex_networks:
                if data_time[name][algo]:
                    data[val][name].append(Integer(round(data_time[name][algo][val][-1], 0)))
                else:
                    data[val][name].append(+oo)

    def my_str(v):
        z = 0
        st = str(v)
        if len(st) > 3:
            if len(st) % 3:
                z = 3 - len(st)%3
                st = st.zfill(len(st) + z)
            st = '\\,'.join(st[i:i+3] for i in range(0, len(st), 3))
        return st[z:]

    def to_str(v, best):
        if v == +oo:
            return '-'
        if v == best:
            return '\\textbf{' + my_str(v) + '}'
        return my_str(v)

    best = {val: {} for val in data}
    for val in data:
        for name in data[val]:
            best[val][name] = min(data[val][name])
    for val in data:
        for name in data[val]:
            data[val][name] = [to_str(v, best[val][name]) for v in data[val][name]]


    s = '\\begin{table}[htbp]\n'
    s += '\\centering\n'
    s += '\\begin{tabular}{|l|l||r|r|r|r|r|r|r|}\n'
    s += '\\hline\n'
    s += '                    	&     & \\multicolumn{1}{c|}{BIOGRID}	& \\multicolumn{1}{c|}{FB}	& \\multicolumn{1}{c|}{P2P}	& \\multicolumn{1}{c|}{DIP}	& \\multicolumn{1}{c|}{CAIDA}	& \\multicolumn{1}{c|}{LOC}\\\\ \\hline\\hline\n'

    for i, algo in enumerate(int_to_algorithm):
        if algo == 'Y':
            continue
        s += '\\multirow{3}{*}{' + algo + '}\t& avg & '
        s += ' & '.join([data['mean'][name][i] for name in complex_networks])
        s += '\\\\ \\cline{2-8}\n'
        s += '                    	& med & '
        s += ' & '.join([data['median'][name][i] for name in complex_networks])
        s += '\\\\ \\cline{2-8}\n'
        s += '                    	& max & '
        s += ' & '.join([data['max'][name][i] for name in complex_networks])
        s += '\\\\\\hline\n\n'
                
    s += '\\end{tabular}\n'
    s += '\\caption{Running time (ms) of the algorithms on Complex networks,  ($k=10,000$)}\n'
    s += '\\label{tab:RunComplex}\n'
    s += '\\end{table}\n'

    with open(target_dir + '/' + 'table_complex_time.tex', 'w') as f:
        f.write(s)

    # TO DO table pour trees !!!!!
    data = {}
    best = {}
    for name in complex_networks:
        data[name] = {algo: Integer(round(data_tree[name][algo]['mean'][-1], 0)) for algo in ['SB', 'PSB', 'PSBv2', 'PSBv3']}
        best[name] = min(data[name].values())
    for name in complex_networks:
        data[name] = {algo: to_str(v, best[name]) for algo, v in data[name].items()}

    s = '\\begin{table}[htbp]\n'
    s += '\\centering\n'
    s += '\\begin{tabular}{|l||r|r|r|r|r|r|r|}\n'
    s += '\\hline\n'
    s += '                  & \\multicolumn{1}{c|}{BIOGRID} & \\multicolumn{1}{c|}{FB}	& \\multicolumn{1}{c|}{P2P} & \\multicolumn{1}{c|}{DIP} & \\multicolumn{1}{c|}{CAIDA} & \\multicolumn{1}{c|}{LOC}\\\\ \\hline\\hline\n'
    s += 'NC, PNC and PNC*  & 1     & 1     & 1     & 1     & 1     & 1\\\\ \\hline\n'
    s += 'SB and SB*        & '
    s += ' & '.join([data[name]['SB'] for name in complex_networks])
    s += '\\\\ \\hline\n'
    s += 'PSB               & '
    s += ' & '.join([data[name]['PSB'] for name in complex_networks])
    s += '\\\\ \\hline\n'
    s += 'PSBv2             & '
    s += ' & '.join([data[name]['PSBv2'] for name in complex_networks])
    s += '\\\\ \\hline\n'
    s += 'PSBv3             & '
    s += ' & '.join([data[name]['PSBv3'] for name in complex_networks])
    s += '\\\\ \\hline\n'
    s += '\\end{tabular}\n'
    s += '\\caption{Average number of stored trees using some $kSSP$ algorithms on complex networks,  ($k=10,000$)}\n'
    s += '\\label{tab:in-branching-complex}\n'
    s += '\\end{table}\n'

    with open(target_dir + '/' + 'table_complex_trees.tex', 'w') as f:
        f.write(s)




def make_all_plots_and_tables(source_dir, target_dir):
    """
    Make all plots
    """
    """
    print("scatter plots algo-algo")
    for name in ['DC', 'COL']:  # networks
        scatter_plot_algo_algo(source_dir, target_dir, name, k=1000)
        scatter_plot_algo_algo_rainbow(source_dir, target_dir, name, k=1000, rainbow=True)
        # # scatter_plot_algo_algo_rainbow(source_dir, target_dir, name, k=1000, rainbow=False)
        scatter_plot_time_per_rank_or_stretch(source_dir, target_dir, name, k=1000)

    print("evolution time per rank (1,000)")
    for name in cities:
        plot_evolution_time_per_rank(source_dir, target_dir, name, k=1000)
        plot_evolution_trees_per_rank(source_dir, target_dir, name, k=1000, calls=False)
        plot_evolution_trees_per_rank(source_dir, target_dir, name, k=1000, calls=True)
    print("evolution time per rank (10,000)")
    for name in complex_networks:
        plot_evolution_time_per_rank(source_dir, target_dir, name, k=10000)
        plot_evolution_trees_per_rank(source_dir, target_dir, name, k=10000, calls=False)
        plot_evolution_trees_per_rank(source_dir, target_dir, name, k=10000, calls=True)

    print("evolution time per k")
    for name in ['DC', 'COL', 'BIOGRID', 'CAIDA', 'LOC']:  # networks
        plot_evolution_running_time_per_k(source_dir, target_dir, name)
    """
    print("tables")
    table_cities(source_dir, target_dir)
    table_complex(source_dir, target_dir)
